# cypress-ts-example

> Docker + Cypress + TypeScript = ❤️

## Docker is simple

You can start using [Cypress](https://www.cypress.io/) quickly if you run
inside a Docker container. You can derive your custom CI image
from [cypress/base](https://hub.docker.com/r/cypress/base/) and install
`cypress`. Here is typical CI file

```yaml
image: cypress/base
cypress-e2e:
  script:
    - npm install -g cypress-cli
    - cypress install --cypress-version 0.19.2
    # run Cypress in headless CI mode
    - cypress run
```

**Hint** it is always a good idea to specify the exact version of a tool
when installing it to avoid breaking changes

### Avoid installing Cypress

For now we have made a Docker image with Cypress pre-installed.
Its tags follow pattern `cypress/internal:cy-<version>` with `<version>`
being the installed Cypress version.

A typical CI file would just contain the test command

```yaml
image: cypress/internal:cy-0.19.2
cypress-e2e:
  stage: test
  script:
    - cypress run
```

**Hint** you can parallelize CI test jobs by defining multiple test jobs
and specifying different spec files to run. For example, if you write two
test files `foo-spec.js` and `bar-spec.js`, you can define two test jobs

```yaml
image: cypress/internal:cy-0.19.2
cypress-e2e-foo:
  stage: test
  script:
    - cypress run --spec cypress/integration/foo-spec.js
cypress-e2e-bar:
  stage: test
  script:
    - cypress run --spec cypress/integration/bar-spec.js
```

## TypeScript

You can enable TypeScript IntelliSense on your Cypress tests. This will make
test writing faster and more error-prone. Just add the TypeScript compiler
to your `package.json` and create the default `tsconfig.json`

```sh
npm i -D typescript
$(npm bin)/tsc --init
```

You can leave default options in the generated `tsconfig.json` file.

### Existing JavaScript files

If you already have `.js` test files, you can add type checking and
IntelliSense support by putting `// @ts-check` on the first line of the `.js`
file. The text editors that support TypeScript, like Visual Studio Code will
start showing red squiggles right away!

![TS checks in JS spec file](images/start-ts-check.png)

The above screenshots shows that we are missing type definitions for
functions `describe`, `it` (which are Mocha test methods) and `cy` object
(which comes from Cypress library). Luckily there are user-contributed types
for both tools.

```sh
npm i -D @types/mocha @types/cypress
```

Reload the project and the red squiggled should go away. Notice that
as you type `cy.` IntelliSense starts showing all Cypress methods you can use!

![Cypress methods](images/cy-methods.png)

As you type method name, you can see its signature and argument types

![Cypress get method](images/cy-get-method.png)

Even the individual properties in the `options` argument are suggested

![Options properties](images/cy-get-method-options.png)

### Transpiling TypeScript

Spec `.ts` files need to be transpiled before running the tests.

## Artifacts

During the CI run, Cypress can [take screenshots and record videos][screenshots].
You can typically store the images and video files as artifacts associated
with the build job. For example

```yaml
cypress-e2e:
  script:
    - cypress run
  artifacts:
    expire_in: 1 week
    paths:
    - cypress/screenshots
    - cypress/videos
```

The GitLab CI shows saved artifacts next to each job

![Cypress artifacts on GitLab CI](images/cypress-ci-artifacts.png)

and you can open each file by browsing to it, for example here is the
screenshot artifact

![Cypress screenshots artifact](images/cypress-ci-screenshot.png)

[screenshots]:https://docs.cypress.io/docs/screenshots-and-videos

## Final thoughts

Running end to end tests using Cypress on your own CI is very simple.
And if you want history / replayable videos / screenshots from all runs,
check out [Cypress Dashboard](https://docs.cypress.io/docs/dashboard-features#section-what-is-the-dashboard-).

Happy testing!

## Big Thanks

Thank you [Gert Hengeveld](https://github.com/ghengeveld)
and [Mike Woudenberg](https://github.com/mikewoudenberg)
for writing and publishing Cypress
[type definitions](https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/cypress)
